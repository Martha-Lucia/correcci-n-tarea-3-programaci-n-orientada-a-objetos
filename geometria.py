# Autora = Martha Cango
# Email = martha.cango@unl.edu.ec

import math


class Punto:
    x = 0
    y = 0

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return '({},{})'.format(self.x, self.y)

    def cuadrante(self):
        if self.x > 0 and self.y > 0:
            return 'I'
        elif self.x < 0 and self.y > 0:
            return 'II'
        elif self.x < 0 and self.y < 0:
            return 'III'
        elif self.x > 0 and self.y < 0:
             return 'IV'
        elif self.x != 0 and self.y == 0:
             return "{} se sitúa sobre el eje X"
        elif self.x == 0 and self.y != 0:
            return "{} se sitúa sobre el eje Y"
        else:
             return "sobre el origen"
        return self.x and self.y

    def vector(self, p):
        vector_resul = Punto(p.x - self.x, p.y - self.y)
        return vector_resul

    def distancia(self, p):
        distan = math.sqrt((p.x - self.x)**2 + (p.y - self.y)**2)
        return distan


class Rectangulo:

    def __init__(self, punto_inicial, punto_final):
        self.punto_inicial = punto_inicial
        self.punto_final = punto_final

    def base(self):
        return self.punto_final.x - self.punto_inicial.x

    def altura(self):
        return self.punto_final.y - self.punto_inicial.y

    def area(self):
        return self.base()*self.altura()


if __name__ == '__main__':

    A = Punto(2, 3)
    B = Punto(5, 5)
    C = Punto(-3, -1)
    D = Punto(0, 0)

    print(f"El punto {A} se encuentra en el cuadrante {A.cuadrante()}")
    print(f"El punto {C} se encuentra en el cuadrante {C.cuadrante()}")
    print(f"El punto {D} se encuentra {D.cuadrante()}")

    print(f"La distancia entre el punto {A} y {B} es: {A.distancia(B)}")
    print(f"La distancia entre el punto {B} y {A} es: {B.distancia(A)}")

    DA = A.distancia(D)
    DB = B.distancia(D)
    DC = C.distancia(D)
    if DA > DB and DA > DC:
        print(f"El punto {A} se encuentra más lejos del origen.")
    elif DB > DA and DB > DC:
        print(f"El punto {B} se encuentra más lejos del origen.")
    else:
        print(f"El punto {C} se encuentra más lejos del origen")

    rsta = Rectangulo(A, B)
    print("La base del rectángulo es: {}".format(rsta.base()))
    print("La altura del rectángulo es: {}".format(rsta.altura()))
    print("El área del rectángulo es: %.2f" % rsta.area())
